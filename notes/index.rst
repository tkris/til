Notes and Howtos
===============

Once in a while I learn new stuff and take notes or write howtos based on these insights.

This page is part of `my til repository on gitlab <https://gitlab.com/tkris/til>`_ where I collect notes, self-written guides, and passed tutorials.

It somewhat reflects what I've been dealing with.



.. toctree::
    :caption: Howtos
    :maxdepth: 2
    :hidden:
    :glob:
    
    guide_*

.. toctree::
    :caption: Notes
    :maxdepth: 2
    :hidden:
    :glob:
   
    note_*

